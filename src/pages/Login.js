import React from "react";
import FormLogin from "../components/LoginPage/FormLogin";

export default function LoginPage({ setAuth, users, setUser }) {
  return (
    <div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
      <div className="col-lg-3">
        <h4 className="text-center fw-bold mb-5">Login Page</h4>
        <FormLogin setAuth={setAuth} users={users} setUser={setUser} />
      </div>
    </div>
  );
}
