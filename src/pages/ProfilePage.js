import { Link } from "react-router-dom";
import Navbar from "../components/Navbar";

export default function ProfilePage({ auth, user, setAuth }) {
  return (
    <>
      <Navbar auth={auth} user={user} setAuth={setAuth} />
      <div className="mt-3 container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <Link to={`/dashboard`} className="text-decoration-none text-dark">
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
        </div>
        <div className="mt-3 row justify-content-center">
          <div className="col-lg-6">
            <table class="table">
              <tbody>
                <tr>
                  <td className="text-muted fw-bolder">Name</td>
                  <td>:</td>
                  <td className="fw-bold">{user.name}</td>
                </tr>
                <tr>
                  <td className="text-muted fw-bolder">Username</td>
                  <td>:</td>
                  <td className="fw-bold">{user.username}</td>
                </tr>
                <tr>
                  <td className="text-muted fw-bolder">Email</td>
                  <td>:</td>
                  <td className="fw-bold">{user.email}</td>
                </tr>
                <tr>
                  <td className="text-muted fw-bolder">Address</td>
                  <td>:</td>
                  <td className="fw-bold">{`${user.address.street}, ${user.address.suite} - ${user.address.city}`}</td>
                </tr>
                <tr>
                  <td className="text-muted fw-bolder">Phone</td>
                  <td>:</td>
                  <td className="fw-bold">{user.phone}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}
