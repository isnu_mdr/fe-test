import axios from "axios";
import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import CommentsContainer from "../components/DetailPostPage/CommentsContainer";
import Navbar from "../components/Navbar";

export default function DetailsPostPage({ auth, user, setAuth }) {
  const [loading, setLoading] = useState(true);
  const [showComments, setShowComments] = useState(false);
  const [detailPost, setDetailPost] = useState({});
  const [author, setAuthor] = useState({});
  const [comments, setComments] = useState([]);

  const { id } = useParams();

  useEffect(() => {
    const fetchDetailPost = async () => {
      await axios
        .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then((res) => {
          setDetailPost(res.data);
          axios
            .get(
              `https://jsonplaceholder.typicode.com/users/${res.data.userId}`
            )
            .then((res) => {
              setAuthor(res.data);
            })
            .catch((err) => {
              console.error(err);
            });
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          console.error(err);
        });
    };

    const fetchComments = async () => {
      await axios
        .get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
        .then((res) => {
          setComments(res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    };

    fetchDetailPost();
    fetchComments();
  }, [id]);

  return (
    <>
      <Navbar auth={auth} user={user} setAuth={setAuth} />
      <div className="mt-3 container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <Link to={`/dashboard`} className="text-decoration-none text-dark">
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
        </div>
        {loading ? (
          <div className="mt-5 justify-content-center text-center">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        ) : (
          <>
            <div className="mt-3 row justify-content-center">
              <div className="col-lg-6">
                <div className="row">
                  <div className="col-lg-4"></div>
                  <div className="col-lg-8">
                    <p className="text-muted fw-semibold">{detailPost.title}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-lg-6">
                <div className="row">
                  <div className="col-lg-4">
                    <h4 className="fw-bolder" style={{ fontSize: "16px" }}>
                      {author?.name}
                    </h4>
                  </div>
                  <div className="col-lg-8">
                    <p className="text-muted">{detailPost.body}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-lg-6">
                <div className="row">
                  <div className="col-lg-4"></div>
                  <div className="col-lg-8">
                    <h4
                      className="fw-semibold"
                      style={{ fontSize: "16px", cursor: "pointer" }}
                      onClick={() => setShowComments((prev) => !prev)}
                    >
                      {showComments ? (
                        `All Comment`
                      ) : (
                        <div className="text-primary">
                          <i className="fa fa-comment"></i>{" "}
                          {comments ? comments.length : 0}
                        </div>
                      )}
                    </h4>
                    {showComments ? (
                      comments.length > 0 ? (
                        <CommentsContainer comments={comments} />
                      ) : (
                        <p>No Comments</p>
                      )
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
}
