import React from "react";
import Navbar from "../components/Navbar";

export default function HomePage({ auth, user, setAuth }) {
  return (
    <div className="vh-100">
      <Navbar auth={auth} user={user} setAuth={setAuth} />
      <div className="d-flex justify-content-center mx-auto">
        <img
          src="bg-hero.jpg"
          class="img-fluid"
          style={{ height: "500px" }}
          alt="Background Jumbotron"
        />
      </div>
    </div>
  );
}
