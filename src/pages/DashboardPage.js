import axios from "axios";
import React, { useState, useEffect } from "react";
import { TablePosts } from "../components/DashboardPage/TablePosts";
import Navbar from "../components/Navbar";
import Pagination from "../components/Pagination";

export default function Dashboard({ auth, user, setAuth }) {
  const [posts, setPosts] = useState([]);
  const [tmpPosts, setTempPosts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [dataPerPage] = useState(10);
  const [loading, setLoading] = useState(true);

  // Get current DATA
  const indexOfLastData = currentPage * dataPerPage;
  const indexOfFirstData = indexOfLastData - dataPerPage;
  const currentData = posts.slice(indexOfFirstData, indexOfLastData);

  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  const paginateFront = () => setCurrentPage(currentPage + 1);
  const paginateBack = () => setCurrentPage(currentPage - 1);

  useEffect(() => {
    const fetchPosts = () => {
      axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((res) => {
          setPosts(res.data);
          setTempPosts(res.data);
          let value = res.data;
          let valueComment = res.data;
          res.data.map((post, index) => {
            axios
              .get(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
              .then((res) => {
                value[index] = { ...value[index], user: res.data.name };
                setPosts(value);
                setTempPosts(value);
              })
              .catch((err) => {
                console.error(err);
              });
            axios
              .get(
                `https://jsonplaceholder.typicode.com/posts/${post.id}/comments`
              )
              .then((res) => {
                valueComment[index] = {
                  ...valueComment[index],
                  comments: res.data,
                };
                setPosts(valueComment);
                setTempPosts(valueComment);
              })
              .catch((err) => {
                console.error(err);
              });
          });
          setLoading(false);
        })
        .catch((err) => {
          console.error(err);
          setLoading(false);
        });
    };

    fetchPosts();
  }, []);

  const postSearch = (keyword) => {
    return tmpPosts.filter((value) => {
      return (
        value.title.toLowerCase().includes(keyword.toLowerCase()) ||
        value.body.toLowerCase().includes(keyword.toLowerCase())
      );
    });
  };

  const handleSearchChange = (e) => {
    setLoading(true);
    let value = e.target.value;
    if (value.length > 0) {
      setPosts(postSearch(value));
      setLoading(false);
    } else {
      setPosts(tmpPosts);
      setLoading(false);
    }
  };

  return (
    <>
      <Navbar auth={auth} user={user} setAuth={setAuth} />
      <div className="mt-3 container d-flex justify-content-center">
        <div className="col-lg-6">
          <div className="search">
            <i className="fa fa-search"></i>
            <input
              type="text"
              className="form-control rounded-5 text-center"
              placeholder="Search"
              onChange={handleSearchChange}
            />
          </div>
          {loading ? (
            <div className="mt-5 justify-content-center text-center">
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          ) : posts.length > 0 ? (
            <>
              <TablePosts posts={currentData} />
              <Pagination
                dataPerPage={dataPerPage}
                totalData={posts.length}
                paginate={paginate}
                paginateBack={paginateBack}
                paginateFront={paginateFront}
                currentPage={currentPage}
              />
            </>
          ) : (
            <div className="row mt-3 text-center">
              <p className="text-muted fw-bold">No Posts</p>
            </div>
          )}
        </div>
      </div>
    </>
  );
}
