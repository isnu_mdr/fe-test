import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css";
import "./assets/css/mystyle.css";
import axios from "axios";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/Login";
import Dashboard from "./pages/DashboardPage";
import DetailsPostPage from "./pages/DetailsPostPage";
import ProfilePage from "./pages/ProfilePage";

function App() {
  const [auth, setAuth] = useState(null);
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({});

  useEffect(() => {
    let user = localStorage.getItem("user");
    user === true ? setAuth(true) : setAuth(false);

    const fetchUsers = async () => {
      await axios
        .get("https://jsonplaceholder.typicode.com/users")
        .then((res) => {
          setUsers(res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    };

    fetchUsers();
  }, []);

  useEffect(() => {
    localStorage.setItem("user", auth);
  }, [auth]);

  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={
            <HomePage auth={auth} logout={() => setAuth(false)} user={user} />
          }
        />
        {!auth && (
          <Route
            path="/login"
            element={
              <LoginPage setAuth={setAuth} users={users} setUser={setUser} />
            }
          />
        )}
        {auth && (
          <>
            <Route
              path="/dashboard"
              element={<Dashboard setAuth={setAuth} auth={auth} user={user} />}
            />
            <Route
              path="/posts/:id"
              element={
                <DetailsPostPage auth={auth} user={user} setAuth={setAuth} />
              }
            />
            <Route
              path="/profile"
              element={
                <ProfilePage auth={auth} user={user} setAuth={setAuth} />
              }
            />
          </>
        )}
        <Route path="*" element={<Navigate to={auth ? "/dashboard" : "/"} />} />
      </Routes>
    </Router>
  );
}

export default App;
