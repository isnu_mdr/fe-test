import React from "react";

const Pagination = ({
  dataPerPage,
  totalData,
  paginate,
  paginateFront,
  paginateBack,
  currentPage,
}) => {
  const totalPages = Math.ceil(totalData / dataPerPage);
  const pageNeighbours = Math.max(0, Math.min(0, 2));
  const totalNumbers = pageNeighbours * 2 + 3;
  const totalBlocks = totalNumbers + 2;

  const LEFT_PAGE = "LEFT";
  const RIGHT_PAGE = "RIGHT";

  const pageNumbers = (from, to, step = 1) => {
    let i = from;
    const pageNumbers = [];

    while (i <= to) {
      pageNumbers.push(i);
      i += step;
    }

    return pageNumbers;
  };

  const fetchPageNumbers = () => {
    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(
        totalPages - 1,
        currentPage + pageNeighbours + 2
      );
      let pages = pageNumbers(startPage, endPage);

      const hasLeftSpill = startPage > 2;
      const hasRightSpill = totalPages - endPage > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        case hasLeftSpill && !hasRightSpill: {
          const extraPages = pageNumbers(
            startPage - spillOffset,
            startPage - 1
          );
          pages = [LEFT_PAGE, ...extraPages, ...pages];
          break;
        }

        case !hasLeftSpill && hasRightSpill: {
          const extraPages = pageNumbers(endPage + 2, endPage + spillOffset);
          pages = [...pages, ...extraPages, RIGHT_PAGE];
          break;
        }

        case hasLeftSpill && hasRightSpill:
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
          break;
        }
      }
      return [1, ...pages, totalPages];
    }

    return pageNumbers(1, totalPages);
  };

  const pages = fetchPageNumbers();
  const handleMoveLeft = () => paginate(currentPage - pageNeighbours * 2 - 1);
  const handleMoveRight = () => paginate(currentPage + pageNeighbours * 2 + 1);

  return (
    <div className="mt-4">
      <nav aria-label="Pagination">
        <ul className="pagination justify-content-center">
          {currentPage === 1 ? (
            <li className="page-item">
              <a className="page-link" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
          ) : (
            <li className="page-item">
              <a
                onClick={() => {
                  paginateBack();
                }}
                className="page-link"
                aria-label="Previous"
              >
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
          )}
          {pages.map((page, i) => {
            if (page === LEFT_PAGE)
              return (
                <li className="page-item">
                  <a key={i} onClick={handleMoveLeft} className="page-link">
                    <span aria-hidden="true">...</span>
                  </a>
                </li>
              );

            if (page === RIGHT_PAGE)
              return (
                <li className="page-item">
                  <a key={i} onClick={handleMoveRight} className="page-link">
                    <span aria-hidden="true">...</span>
                  </a>
                </li>
              );

            return (
              <li className="page-item">
                <a
                  onClick={() => {
                    paginate(page);
                  }}
                  key={i}
                  className={`page-link ${currentPage === page && "active"}`}
                >
                  {page}
                </a>
              </li>
            );
          })}
          <li className="page-item">
            {totalPages - currentPage === 0 ? (
              <a className="page-link">&raquo;</a>
            ) : (
              <a
                onClick={() => {
                  paginateFront();
                }}
                className="page-link"
              >
                <span>&raquo;</span>
              </a>
            )}
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Pagination;
