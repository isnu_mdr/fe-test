import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

export default function FormLogin({ setAuth, users, setUser }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    const checkUsername = users.find(
      (user) => user.username.toLowerCase() === username.toLowerCase()
    );

    if (username === password && checkUsername) {
      setUser(checkUsername);
      setAuth(true);
      navigate("dashboard");
    } else {
      setAuth(false);
      navigate("/");
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-3">
        <input
          type="text"
          className="form-control text-center rounded-5"
          placeholder="username"
          name="username"
          onChange={(e) => setUsername(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <input
          type="password"
          className="form-control text-center rounded-5"
          placeholder="password"
          name="password"
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <div className="d-grid">
        <button className="btn btn-primary fw-bold rounded-5" type="submit">
          Sign in
        </button>
        <Link to={"/"} className="mt-2 btn btn-danger fw-bold rounded-5">
          Back to Home
        </Link>
      </div>
    </form>
  );
}
