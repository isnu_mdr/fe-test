import React from "react";

export default function CommentsContainer({ comments }) {
  return comments.map((comment) => (
    <div className="mt-3 row" key={comment.id}>
      <div className="col-lg-4">
        <h4 className="fw-bolder" style={{ fontSize: "16px" }}>
          {comment.name}
        </h4>
      </div>
      <div className="col-lg-8">
        <p className="text-muted">{comment.body}</p>
      </div>
    </div>
  ));
}
