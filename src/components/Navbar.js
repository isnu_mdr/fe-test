import React, { useState } from "react";
import { Link } from "react-router-dom";

export default function Navbar({ auth, setAuth, user }) {
  const [showDropdown, setShowDropdown] = useState(false);

  return (
    <nav className="navbar">
      <div className="container">
        <Link to={"/"} className="navbar-brand fw-bold">
          Cinta Coding
        </Link>
        {auth && (
          <Link
            to={"/dashboard"}
            className="text-muted fw-bolder text-decoration-none border-bottom border-3 border-primary px-3"
            style={{
              fontSize: "20px",
            }}
          >
            Post
          </Link>
        )}
        <div className="d-flex">
          {auth ? (
            <>
              <Link
                to={"/dashboard"}
                className="fw-bold text-dark text-decoration-none"
                style={{ fontSize: "20px", marginRight: "8px" }}
              >
                Welcome,
              </Link>
              <div
                className={`dropdown ${showDropdown && "show"}`}
                onMouseLeave={() => setShowDropdown(false)}
              >
                <div
                  className="fw-bold text-primary text-decoration-none dropdown-toggle"
                  style={{ fontSize: "20px", cursor: "pointer" }}
                  onClick={() => setShowDropdown((prev) => !prev)}
                >
                  {user?.name.split(" ").slice(0, -1).join(" ")}
                </div>
                <ul className={`dropdown-menu ${showDropdown && "show"}`}>
                  <li>
                    <Link to={"/profile"} className="dropdown-item">
                      Profile
                    </Link>
                  </li>
                  <li>
                    <button
                      className="dropdown-item"
                      onClick={() => setAuth(false)}
                    >
                      Logout
                    </button>
                  </li>
                </ul>
              </div>
            </>
          ) : (
            <Link
              to={"/login"}
              className="btn btn-primary rounded-5 px-4 fw-bold"
            >
              Login
            </Link>
          )}
        </div>
      </div>
    </nav>
  );
}
