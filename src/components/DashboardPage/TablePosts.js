import React from "react";
import { Link } from "react-router-dom";

export const TablePosts = ({ posts }) => {
  return (
    <>
      {posts.map((post, index) => (
        <div className="row mt-3" key={post.id}>
          <div className="col-lg-4">
            <h4 className="fw-bolder" style={{ fontSize: "16px" }}>
              {post.user}
            </h4>
          </div>
          <div className="col-lg-8">
            <p className="text-muted">{post.body}</p>
            <div className="d-flex gap-4 text-primary fw-bold">
              <div>
                <i className="fa fa-comment"></i>{" "}
                {post.comments ? post.comments.length : 0}
              </div>
              <div>
                <Link
                  to={`/posts/${post.id}`}
                  className="fw-bold text-decoration-none"
                >
                  Detail
                </Link>
              </div>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};
